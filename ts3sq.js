var net = require('net')
 , binary = require('binary')
 , events = require('events')
 , sys = require('sys');

 function ServerQuery(host, port) {
	events.EventEmitter.call(this);

	var ready = false;
	var commandQueue = [];
	var self = this;
	var client = net.connect(port, host);

	client.on('close', function(whyyy) {
		self.emit('close', whyyy);
	});

	client.on('error', function(error) {
		self.emit('error', error);
	});

	var b = binary()
		.scan('line', new Buffer('\n'))
		.loop(function (end,vars) {
			var line = vars.line.toString();
			line = line.trim();

			if (ready)
				onLine(line);
			//Hacky. But works.
			if (line.indexOf("Welcome") == 0) {
				ready = true;
				self.emit('ready');
			}

			this.scan('line', new Buffer('\n'))
		});

	client.pipe(b);

	this.execute = function(command, cb) {
		commandQueue.push({command: command, cb: cb, sent: false});
	}

	function onLine(line) {
		if (line.indexOf("err") == 0) {
			commandQueue[0].err = parseTs3(line);
			var element = commandQueue.shift();
			if (typeof element.cb == "function")
				element.cb(element);
			return;
		}
		if (line.indexOf("notify") == 0) {
			var partPos = line.indexOf(' ');
			var body = parseTs3(line.substring(partPos + 1));
			var type = line.substring(0, partPos);
			self.emit('notify', {type: type, body: body});
			return;
		}
		//This is probably a response, since the server will send another line with the error code, don't shift and check for a CB
		commandQueue[0].response = parseTs3(line);
	}

	setInterval(function() {
		if (commandQueue.length == 0)
			return;
		if (commandQueue[0].sent == false) {
			commandQueue[0].sent = true;
			client.write(commandQueue[0].command.trim() + "\n", "utf8");
		}
	}, 10)

	function parseTs3(body) {
		//NEVER TOUCH
		var nBody = [];
		var items = body.split('|');
		for (var i in items) {
			var elements = items[i].split(' ');
			nBody[i] = {};
			for (var i2 in elements) {
				var element = elements[i2].split('=');
				if (typeof element[1] != "undefined") {
					//unescape whitespaces, pipes and slashes
					element[1] = element[1].replace(/\\s/g, ' ').replace(/\\p/g, '|').replace(/\\\\/g, '\\').replace(/\\\//g, '/');
				}
				nBody[i][element[0]] = element[1];
			}
		}
		return nBody;
	}
}

function escapeString(string) {
	return string.replace(/ /g, "\\s").replace(/\//g, "\\/"); // Just spaces cause I'm lazy, send me a patch or something if you needed more than that
}

sys.inherits(ServerQuery, events.EventEmitter);

exports.ServerQuery = ServerQuery;
exports.escapeString = escapeString;
