node-ts3sq
==========
node-ts3sq is a simple library to interact with the TeamSpeak3 ServerQuery Interface [documented here](http://media.teamspeak.com/ts3_literature/TeamSpeak%203%20Server%20Query%20Manual.pdf).

Please do not use this library in production without careful review. I created it for my own needs and it's largely untested.
Let me know if it ends up being used somewhere else!

Code Example
============

	var ts = require('ts3sq');
	//Lets connect for a start...
	var client = new ts.ServerQuery('localhost', 10011);
	client.on('ready', function() {
		//...login...
		client.execute('login <username> <passwprd>');
		//...and switch to instance 4, better check if this was successful, so we use a callback this time.
		client.execute('use 4', function(element) {
			//element always has an "err" property, just like the query itself always returns an error, even if the action was successful.
		});
		//The first TS3 library for node to support notifications, the SQ isn't just request-reponse, hooray!
		client.execute('servernotifyregister event=server');
	});
	client.on('notify', function(notification) {
		if (notification.type == "notifycliententerview") {
			console.log(notification.body[0].client_nickname + " has connected");
		}
	})

	client.on('error', function(error){
		// This is usually an error right from the TCP socket, the close event will most likely follow on the next tick
	})

	client.on('close', function(){
		// Connection closed, for whatever reason.
	})

Installation
============
<code>npm install ts3sq</code>